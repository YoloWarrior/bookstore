namespace BookStore.DAL.Migrations
{
    using BookStore.DAL.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BookStore.DAL.EF.BookContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BookStore.DAL.EF.BookContext context)
        {
            context.Books.AddOrUpdate(new Book { Id = 1, Name = "Romeo and Juliet", Description = "is a tragedy written by William Shakespeare early in his career about two young star-crossed lovers whose deaths ultimately reconcile their feuding families. It was among Shakespeare's most popular plays during his lifetime and along with Hamlet, is one of his most frequently performed plays. Today, the title characters are regarded as archetypal young lovers.", AuthorId = 1, CategoryId = 2, Price = 30});
            context.Books.AddOrUpdate(new Book { Id = 2, Name = "The Call of Cthulhu", Description = "Of such great powers or beings there may be conceivably a survival . . . a survival of a hugely remote period when . . . consciousness was manifested, perhaps, in shapes and forms long since withdrawn before the tide of advancing humanity . . . forms of which poetry and legend alone have caught a flying memory and called them gods, monsters, mythical beings of all sorts and kinds. . . .", AuthorId = 1, CategoryId = 1 ,Price = 40});
           

            //End Books Initialize

            //Start Authors Initialize
            context.Authors.AddOrUpdate(new Author { Id = 1, Name = "William Shakespeare" });
            context.Authors.AddOrUpdate(new Author { Id = 2, Name = "H. P. Lovecraft" });
            //End Authors Initialize

            //Start Categories Initialize
            context.Genres.AddOrUpdate(new Genre { Id = 1, CategoryName = "Horror" });
            context.Genres.AddOrUpdate(new Genre { Id = 2, CategoryName = "Dramma" });
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
